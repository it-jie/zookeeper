package com.baizhi;

import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.serialize.SerializableSerializer;

public class ConnectionTest {

    public static void main(String[] args) {
        ZkClient zkClient = new ZkClient("192.168.129.129:2181", 6000*30, 60000, new SerializableSerializer());
        System.out.println(zkClient);
        zkClient.close();
    }
}
