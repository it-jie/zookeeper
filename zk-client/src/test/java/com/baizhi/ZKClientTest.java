package com.baizhi;

import com.baizhi.entity.User;
import org.I0Itec.zkclient.IZkChildListener;
import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.serialize.SerializableSerializer;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class ZKClientTest {

    private ZkClient zkClient;

    // 1、创建节点
    @Test
    public void testCreateNode() {
        // 1.持久节点
        zkClient.create("/node1", "xiaochen", CreateMode.PERSISTENT);
        // 2.持久顺序节点
        zkClient.create("/node1/names", "zhangsan", CreateMode.PERSISTENT_SEQUENTIAL);
        // 3.临时节点
        zkClient.create("/node1/lists", "xiaoxiao", CreateMode.EPHEMERAL);
        // 4.持久顺序节点
        zkClient.create("/node1/list11", "xiaoming", CreateMode.EPHEMERAL_SEQUENTIAL);
    }

    // 2、删除节点
    @Test
    public void testDeleteNode() {
        // 删除没有子节点的节点
//        boolean delete = zkClient.delete("/node1");
//        System.out.println(delete);
        // 递归删除节点信息
        boolean recursive = zkClient.deleteRecursive("/node1");
        System.out.println(recursive);
    }

    // 3、查看当前节点下所有子节点
    @Test
    public void testFindNodes() {
        List<String> children = zkClient.getChildren("/");
        for (String child : children) {
            System.out.println(child);
        }
    }

    // 4、查看某个节点数据
    // 注意：通过java客户端操作，需要保证节点存储和节点获取时的序列化方式必须一致
    @Test
    public void testFindNodeData() {
        Object readData = zkClient.readData("/node1");
        System.out.println(readData);
    }

    // 5、查看节点状态信息
    @Test
    public void testFindNodeStat() {
        Stat stat = new Stat();
        Object readData = zkClient.readData("/node1", stat);
        System.out.println(readData);
        System.out.println(stat.getCversion());
        System.out.println(stat.getCtime());
        System.out.println(stat.getCzxid());
    }

    // 6、修改节点数据
    @Test
    public void testWriteData() {
        User user = new User(1, "小陈", 23, new Date());
        zkClient.writeData("/node1", user);
    }

    // 7、监听节点数据变化
    // 注意：1.永久监听    2.只有通过java客户端修改才会触发
    @Test
    public void testWatchDataChange() throws IOException {
        IZkDataListener listener = new IZkDataListener() {
            // 当前节点数据变化时，触发对应这个方法
            public void handleDataChange(String dataPath, Object data) throws Exception {
                System.out.println("当前节点路径：" + dataPath);
                System.out.println("当前节点变化后的数据：" + data);
            }
            // 当前节点删除时，触发这个方法
            public void handleDataDeleted(String dataPath) throws Exception {
                System.out.println("当前节点路径：" + dataPath);
            }
        };
        zkClient.subscribeDataChanges("/node1", listener);
        System.in.read();   // 阻塞当前监听
    }

    // 8、监听节点目录变化
    @Test
    public void testWatchPathChange() throws IOException {
        IZkChildListener listener = new IZkChildListener() {
            /**
             * 当节点目录发生变化时，会自动调用这个方法
             * 参数1：父节点名称
             * 参数2：父节点中的所有子节点名称
             */
            public void handleChildChange(String parentPath, List<String> currentChilds) throws Exception {
                System.out.println("父节点名称：" + parentPath);
                System.out.println("发生变更后子节点名称：" + currentChilds);
            }
        };
        zkClient.subscribeChildChanges("/node1", listener);
        System.in.read();   // 阻塞当前监听
    }


    // 初始化客户端对象
    @Before
    public void before() {
        /**
         * 获取连接
         * 参数1：zkserver服务器ip:port
         * 参数2：会话超时时间
         * 参数3：连接超时时间
         * 参数4：序列化方式
         */
        // zkClient = new ZkClient("192.168.129.129:2181", 6000*30, 60000, new SerializableSerializer());

        // zk集群
        zkClient = new ZkClient("192.168.129.129:3001,192.168.129.129:4001,192.168.129.129:5001", 6000*30, 60000, new SerializableSerializer());
    }

    @After
    public void after() throws InterruptedException {
        // 等待10s
        // Thread.sleep(10000);
        // 释放资源
        zkClient.close();
    }
}
